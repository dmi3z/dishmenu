import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'datePipe'
})

export class DatePipe implements PipeTransform {
  transform(value: string) {
    moment.locale('ru');
    return moment(value, 'YYYY-MM-DD').format('ll');
  }
}
