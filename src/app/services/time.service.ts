import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { Subject } from 'rxjs';

@Injectable()

export class TimeService {

  public timer = new Subject<number>();
  private readonly timeInterval = 1000;

  constructor() {}

  get currentTime(): number {
    return moment().unix();
  }

  startTimer(): void {
    setInterval(() => {
      this.timer.next(this.currentTime);
    }, this.timeInterval);
  }

}
