import { LoginPageComponent } from './pages/login/login.component';
import { ChannelsPageComponent } from './pages/channels/channels.component';
import { HeaderComponent } from './components/header/header.component';
import { DishComponent } from './components/dish/dish.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { OrderComponent } from './components/order/order.component';
import { ModalComponent } from './components/modal/modal.component';
import { MenuPageComponent } from './pages/menu/menu.component';
import { RoutingModule } from './routing.module';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';
import { ChannelComponent } from './pages/channels/channel/channel.component';
import { ScalingSquaresSpinnerModule } from 'angular-epic-spinners';
import { DateComponent } from './pages/channels/date/date.component';

import { TvshowComponent } from './pages/channels/tvshow/tvshow.component';

import { TimePipe, DatePipe } from './pipes';

import {
  AuthService,
  DataService,
  TimeService,
  LoginGuard,
  AuthGuard,
  LoadingService
} from './services';
import { ColorDirective } from './directives/color.directive';
import { ClickDirective } from './directives/click.directive';


@NgModule({
  declarations: [
    AppComponent,
    DishComponent,
    OrderComponent,
    ModalComponent,
    HeaderComponent,
    MenuPageComponent,
    LoginPageComponent,
    ChannelsPageComponent,
    ChannelComponent,
    DateComponent,
    DatePipe,
    TimePipe,
    TvshowComponent,
    ColorDirective,
    ClickDirective
  ],
  imports: [
    BrowserModule,
    RoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ScalingSquaresSpinnerModule
  ],
  providers: [
    AuthService,
    AuthGuard,
    LoginGuard,
    DataService,
    LoadingService,
    TimeService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
