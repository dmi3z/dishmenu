import { Channel } from './../../../models/channels.model';
import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-channel',
  templateUrl: './channel.component.html',
  styleUrls: ['./channel.component.scss']
})

export class ChannelComponent {

  static activeId: number;

  @Input() num: number;
  @Input() channel: Channel;
  @Output() channelSelectEvent = new EventEmitter<number>();


  constructor() {}

  public loadTvshows(): void {
    ChannelComponent.activeId = this.channel.channel_id;
    this.channelSelectEvent.emit(this.channel.channel_id);
  }

  public get isActive(): boolean {
    return this.channel.channel_id === ChannelComponent.activeId;
  }
}
