import { Component, Input, ViewChild, ElementRef, Output, EventEmitter, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-date',
  templateUrl: './date.component.html',
  styleUrls: ['./date.component.scss']
})

export class DateComponent implements AfterViewInit {

  static activeDate: string;

  @Input() date: string;
  @ViewChild('current', { static: false }) cur: ElementRef;
  @Output() dateEvent = new EventEmitter<string>();

  constructor() { }

  ngAfterViewInit() {
    if (this.isActive) {
      setTimeout(() => {
        const element: HTMLDivElement = this.cur.nativeElement;
        console.log(element);
        element.scrollIntoView({ behavior: 'smooth' });
      }, 500);
    }
  }

  public get isActive(): boolean {
    return this.date === DateComponent.activeDate;
  }

  public setActivedate(): void {
    DateComponent.activeDate = this.date;
    this.dateEvent.emit(this.date);
  }
}
