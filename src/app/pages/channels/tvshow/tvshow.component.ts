import { TimeService } from './../../../services/time.service';
import { Component, Input, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { Tvshow } from 'src/app/models/channels.model';

@Component({
  selector: 'app-tvshow',
  templateUrl: './tvshow.component.html',
  styleUrls: ['./tvshow.component.scss']
})

export class TvshowComponent implements AfterViewInit {

  @Input() tvshow: Tvshow;
  @Input() currentTime: number;
  @ViewChild('currentShow', { static: false }) current: ElementRef;

  constructor() {}

  ngAfterViewInit() {
    if (this.isCurrent) {
      setTimeout(() => {
        const elem: HTMLDivElement = this.current.nativeElement;
        elem.scrollIntoView({ behavior: 'smooth' });
      }, 100);
    }
  }

  public get isCurrent(): boolean {
    return (this.tvshow.start <= this.currentTime && this.tvshow.stop > this.currentTime);
  }

  public get progress(): number {
    return Math.round(((this.currentTime - this.tvshow.start) / (this.tvshow.stop - this.tvshow.start)) * 10000) / 100;
  }
}
