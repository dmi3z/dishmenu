import { Directive, ElementRef, Renderer2, HostListener, AfterViewInit } from '@angular/core';

@Directive({
  selector: '[appClick]'
})

export class ClickDirective implements AfterViewInit {

  private defaultColor: string;

  constructor(private element: ElementRef, private renderer: Renderer2) {}

  @HostListener('click') onElementClick() {
    const item: HTMLElement = this.element.nativeElement;
    const currentColor = item.style.background;
    if (currentColor !== this.defaultColor) {
      this.renderer.setStyle(item, 'background', this.defaultColor);
    } else {
      this.renderer.setStyle(item, 'background', 'lightgreen');
    }
  }

  ngAfterViewInit() {
    this.defaultColor = this.element.nativeElement.style.background;
  }

}
