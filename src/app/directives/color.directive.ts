import { Directive, ElementRef, OnInit, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appColor]'
})

export class ColorDirective implements OnInit {

  constructor(private element: ElementRef, private renderer: Renderer2) {}

  ngOnInit() {
    const item: HTMLElement = this.element.nativeElement;
    this.renderer.setStyle(item, 'color', 'red');
  }
}
